#!/bin/mksh
nsleep="$(cat nsleep)"
echo "STARTING with NSLEEP $nsleep"

sleep $nsleep
echo "AWOKEN. EXITING WITH ERROR"

exit 1
