#define _XOPEN_SOURCE 700
#define _FORTIFY_SOURCE 2

/* This is licensed under the GPLv2.
   Please compile with:

   gcc -std=c99 -Wall -pedantic restart.c -o restart

   Thanks :)
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <fenv.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>

const char *helpstr =                                                                             \
"restart [-r n|-t ms] <program> [args]\n\n"                                                       \
"Simple program to restart a failed program given a failure.\n"                                   \
"It is poll-free (And therefore light on system resources) and will detect\n"                     \
"and die upon detecting restart loops, thus ensuring that system resources\n"                     \
"do not lock up.\n"                                                                               \
"\nOptional Arguments:\n"                                                                         \
"    -t <ms>\n"                                                                                   \
"\tMaximum number of seconds between restarts before it is considered a restart-loop\n"           \
"\t(Default is 2 seconds).\n"                                                                     \
"    -r <n>\n"                                                                                    \
"\tNumber of restarts before it is considered a restart-loop\n"                                   \
"\t(Default is 5 restarts).\n"                                                                    \
"\n\n"                                                                                            \
"For support please email finnoleary@inventati.org\n"                                             \
"Written: 2018-11-16T06:14:00Z\n";

long brestarts = 2;
long nrestarts = 5;

int doprog(char **plist)
{
	pid_t p = -1;
	int status = 0;

	if ((p = fork()) < 0) {
		perror("fork");
		exit(1);
	}
	else if (p == 0) { /* child process */
		if ((execvp(plist[0], plist)) < 0) {
			perror("execvp");
			exit(1);
		}
	}
	else { /* parent process */
		do {
			p = wait(&status);
		} while (p < 0 && errno == EINTR);

		if (p < 0) {
			perror("waitpid");
			exit(1);
		}
		return WIFEXITED(status);
	}
	exit(0); /* Should NEVER reach this point */
}

long tosec(struct timespec *ts)
{
	long ns = 0;
	if ((feclearexcept(FE_ALL_EXCEPT)) < 0) { perror("feclearexcept"); exit(1); }
	if ((ns = lrint((double)ts->tv_nsec / 1000000000.0)) < 0) { perror("lrint"); exit(1); }
	return ts->tv_sec + ns;
}

int timeloop(char **plist)
{
	long restarts = 0;
	long dt = 0;
	struct timespec start, end;

	while (restarts < nrestarts) {
		if ((clock_gettime(CLOCK_MONOTONIC, &start)) < 0) {
			perror("clock_gettime"); exit(1);
		}

		if (doprog(plist)) { break; }

		if ((clock_gettime(CLOCK_MONOTONIC, &end)) < 0) {
			perror("clock_gettime"); exit(1);
		}

		dt = tosec(&end) - tosec(&start);
		if (dt < brestarts) {
			restarts++;
		}
		else {
			restarts = 0;
		}
	}
	return (restarts < nrestarts);
}

int main(int argc, char **argv)
{
	int i;
	for (i = 1; i < argc; i++) {
		if (*argv[i] != '-') { break; }
		if (!argv[i][1]) {
			printf("What are you trying to do with standard input? It's not valid here\n");
			exit(1);
		}
		else if (argv[i][1] == 't' && !argv[i][2] && argv[i+1]) {
			if ((brestarts = atol(argv[++i])) < 1) {
				printf("Argument cannot be zero or negative: %s\n", argv[i-1]);
				exit(1);
			}
		}
		else if (argv[i][1] == 'r' && !argv[i][2] && argv[i+1]) {
			if ((nrestarts = atol(argv[++i])) < 1) {
				printf("Argument cannot be zero or negative: %s\n", argv[i-1]);
				exit(1);
			}
		}
		else if (argv[i][1] == 'h' && !argv[i][2]) {
			printf("%s", helpstr);
			exit(1);
		}
		else {
			printf("Invalid argument: %s\n", argv[i]);
			exit(1);
		}
	}
	if (!argv[i]) { printf("No program supplied.\n"); printf("%s", helpstr); exit(1); }

	if (!timeloop(argv+i)) {
		printf("Went into crash restart loop. Exiting.\n");
	}
	return 0;
}

